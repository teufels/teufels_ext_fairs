<?php

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}


$GLOBALS['TCA']['tx_teufelsfairs_domain_model_fair']['types']['1']['showitem'] = '--div--;General,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource,
--div--;Messe,logo, title, start, end, city, location, details;;;richtext:rte_transform[mode=ts_links], link, file, 
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden;;1, starttime, endtime';



$GLOBALS['TCA']['tx_teufelsfairs_domain_model_fair']['columns']['link']['config']['wizards'] = array(
	'_PADDING' => 2,
	'link' => array(
		'type' => 'popup',
		'title' => 'Link',
		'icon' => 'link_popup.gif',
		'module' => array(
			'name' => 'wizard_element_browser',
			'urlParameters' => array(
				'mode' => 'wizard'
			)
		),
		'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1'
	)
);

$ConfigsPaletteImage = array(
	'0' => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
);
$GLOBALS['TCA']['tx_teufelsfairs_domain_model_fair']['columns']['logo']['config']['foreign_types'] = $ConfigsPaletteImage;


$GLOBALS['TCA']['tx_teufelsfairs_domain_model_fair']['ctrl']['label'] = 'title';
$GLOBALS['TCA']['tx_teufelsfairs_domain_model_fair']['ctrl']['label_alt'] = 'city';
$GLOBALS['TCA']['tx_teufelsfairs_domain_model_fair']['ctrl']['label_alt_force'] =  1;







