<?php
namespace TEUFELS\TeufelsFairs\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class TEUFELS\TeufelsFairs\Controller\FairController.
 *
 * @author Dominik Hilser <d.hilser@teufels.com>
 */
class FairControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \TEUFELS\TeufelsFairs\Controller\FairController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('TEUFELS\\TeufelsFairs\\Controller\\FairController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllFairsFromRepositoryAndAssignsThemToView()
	{

		$allFairs = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$fairRepository = $this->getMock('TEUFELS\\TeufelsFairs\\Domain\\Repository\\FairRepository', array('findAll'), array(), '', FALSE);
		$fairRepository->expects($this->once())->method('findAll')->will($this->returnValue($allFairs));
		$this->inject($this->subject, 'fairRepository', $fairRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('fairs', $allFairs);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenFairToView()
	{
		$fair = new \TEUFELS\TeufelsFairs\Domain\Model\Fair();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('fair', $fair);

		$this->subject->showAction($fair);
	}
}
