<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "teufels_fairs"
 *
 * Auto generated by Extension Builder 2016-11-03
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'teufels_fairs',
	'description' => 'List of fairs',
	'category' => 'plugin',
	'author' => 'Dominik Hilser',
	'author_email' => 'd.hilser@teufels.com',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '0.0.1',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-7.6.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);